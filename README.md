Ultimate Math Tool (by FanFreak247)

Language: Python

This is a tool (directed towards high school students for now) that allows students to check their work, and calculate things that they might need. 

**Current Functionality**
- Calculate Area/Perimeter of Rectangle, Trapezoid, Triangle and Regular Polygons.
- Find Midpoint, Equation, and Right Bisector Equation given two points.

**Planned Functionality**
- Solve Equations
- Circumcenter
- Expansion & Simplification
- GUI (it's console for now)